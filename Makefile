medlemslapp.pdf: medlemslapp.tex
	latexmk -pdf $<

utskrift-framsida.pdf: medlemslapp.pdf
	pdfjam $< 1 $< 1 $< 1 $< 1 --nup 2x2 --outfile $@

utskrift-baksida.pdf: medlemslapp.pdf
	pdfjam $< 2 $< 2 $< 2 $< 2 --nup 2x2 --outfile $@

utskrift.pdf: utskrift-framsida.pdf utskrift-baksida.pdf
	pdfjam $< --outfile $@

clean:
	latexmk -CA
